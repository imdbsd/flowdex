// @flow
async function getPokemons(cursor: string) {
  try {
    let url = 'https://pokeapi.co/api/v2/pokemon?limit=10'
    if(cursor) {
      url = cursor
    }
    const request = await fetch(url);
    const response = await request.json();

    return response;
  } catch (e) {
    console.log(e);
    return false;
  }
}

export default getPokemons;
