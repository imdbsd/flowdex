// @flow
async function getPokemon(name: string) {
  try {
    const request = await fetch(`https://pokeapi.co/api/v2/pokemon/${name}`);
    const response = await request.json();
    
    return response;
  } catch (e) {
    console.log(e);
    return false
  }
}

export default getPokemon;
