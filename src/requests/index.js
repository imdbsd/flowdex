import getPokemon from './getPokemon'
import getPokemons from './getPokemons'

export {
    getPokemon,
    getPokemons
}