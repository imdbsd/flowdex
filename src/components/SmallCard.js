function SmallCard(props) {
    let element = document.createElement('article')
    element.className = 'small-card'
    element.innerHTML = `&bull; ${props.name}`
    element.onclick = function() {
        console.log(props.url)
    }
    return element
}

export default SmallCard