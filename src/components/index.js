import Pagination from './Pagination'
import SmallCardList from './SmallCardList'

export {
    Pagination,
    SmallCardList
}