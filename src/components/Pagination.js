function Pagination(props) {
    const pagination = document.getElementById('pagination')
    pagination.innerHTML = ''
    const buttonPrev = document.createElement('button')
    buttonPrev.className = 'button'
    buttonPrev.innerHTML = '&lt;'
    buttonPrev.onclick = () => props.handlePaginate(props.previous)
    if(props.previous === null) {
        buttonPrev.disabled = true
    }
    const buttonNext = document.createElement('button')
    buttonNext.className = 'button'
    buttonNext.innerHTML = '&gt;'
    buttonNext.onclick = () => props.handlePaginate(props.next)
    if(props.next === null) {
        buttonNext.disabled = true
    }
    pagination.appendChild(buttonPrev)
    pagination.appendChild(buttonNext)
}

export default Pagination