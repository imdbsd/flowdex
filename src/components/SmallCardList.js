import SmallCard from './SmallCard'
import Pagination from './Pagination'
import { getPokemons, getPokemon } from '../requests'

function SmallCardList(state, name) {
   if(state === 'load') {
    const load = (cursor) => {
        const container = document.getElementById('card-container')
        console.log({cursor})
        container.innerHTML = ''
        getPokemons(cursor)
        .then(response => {
            console.log(container)
            console.log(response)
            response.results.forEach(result => {
                const card = SmallCard({name: result.name, url: result.url})
                container.append(card)
            })
            Pagination({...response, handlePaginate: (cursor) => load(cursor)})
        })
    }
    load()
   }
   else if(state === 'search') {
       const load = () => {
            const container = document.getElementById('card-container')
            // console.log({cursor})
            container.innerHTML = ''
            getPokemon(name)
            .then(response => {
                console.log(container)
                console.log(response)
                const card = SmallCard({name: response.species.name, url: response.species.url})
                container.append(card)
            })
       }
       load()
   }
}

export default SmallCardList