// @flow
import {SmallCardList} from './components'

window.onload = function() {
    SmallCardList('load')

    const search = document.getElementById('search-bar')
    if(search) {
        search.addEventListener('submit', function(e: any) {
            e.preventDefault()
            const input: any = document.getElementById('name')
            if(input) {
                SmallCardList('search', input.value)
            }
        })
    }    
}