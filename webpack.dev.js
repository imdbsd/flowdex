const merge = require('webpack-merge')
const path = require('path')
const config = require('./webpack.common.js')

console.log('running development webpack config...')

module.exports = merge(config, {
    mode: 'development',
    output: {
        filename: '[name].js',
        publicPath: '/scripts/dist/',
        path: path.resolve(__dirname, 'public/dist')
    },  
})