const path = require('path')
const HTMLwebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: ['babel-polyfill', path.resolve(__dirname, 'src')],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                  loader: 'babel-loader'
                }
            },        
        ]
    },
    plugins: [],
    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all'
                }
            }
        }
    }
}